from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

mhs_name = 'Rakha Rayhan Nusyura'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002,1,21)
npm = 2006530141

# index function
@login_required(None, 'next', '/admin/login')
def index(request):
    friend = Friend.objects.all()
    response = {'friends':friend}
    return render(request, 'lab3_index.html', response)

# calculate age function
def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

# add friend function
@login_required(None, 'next', '/admin/login')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3')
    return render(request, 'lab3_form.html')