from django.urls import path
from .views import index, add_friend

urlpatterns = [
    # add 'add' path
    path('', index, name='index'),
    path('add', add_friend)
]