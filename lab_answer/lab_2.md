1.  Apakah perbedaan antara JSON dan XML?
    a. JSON tidak memiliki tag awal dan akhir serta sintaksnya lebih ringan, sementara XML menggunakan lebih banyak karakter untuk mewakili data yang sama.
    b. JSON cenderung sangat cepat dalam penguraian karena ukuran file-nya yang kecil, sementara XML cenderung lambat dalam penguraingan karena ukuran file-nya yang besar.
    c. JSON hanyalah sebuah format data, sementara XML adalah bahasa markup.
    d. JSON menyimpan datanya seperti map, sementara XML menyimpan datanya sebagai tree structure
    e. JSON hanya mendukung tipe data primitif dan objek, sementara XML mendukung banyak tipe data kompleks dan nonprimitif.


2.  Apakah perbedaan antara HTML dan XML?
    a. HTML berfokus pada penyajian data, sementara XML berfokus pada transfer data.
    b. HTML case-insensitive, sementara XML case-sensitive.
    c. HTML tidak strict dalam penggunaan closing tag, sementara XML strict.
    d. HTML dapat mengabaikan error kecil, sementara XML tidak memperbolehkan adanya error.
    e. HTML membantu mengembangkan struktur webpage, sementara XML membantu untuk bertukar data di antara berbagai platform.