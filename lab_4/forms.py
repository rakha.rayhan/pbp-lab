from django import forms
from lab_2.models import Note

# NoteForm class
class NoteForm(forms.ModelForm):
    class Meta():
        model = Note
        fields = "__all__"