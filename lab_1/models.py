from django.db import models

# create Friend model that contains name, npm, and DOB (date of birth)

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    dob = models.DateField()
    # implement missing attributes in Friend model
