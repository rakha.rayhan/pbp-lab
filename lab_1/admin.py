from django.contrib import admin
from .models import Friend

# register Friend model
admin.site.register(Friend)