import 'package:flutter/material.dart';

class CatatanPenyediaForm extends StatefulWidget {
  final String title;
  const CatatanPenyediaForm({Key? key, required this.title}) : super(key: key);
  @override
  _CatatanPenyediaFormState createState() => _CatatanPenyediaFormState();
}

class _CatatanPenyediaFormState extends State<CatatanPenyediaForm> {
  String _judulCatatan = "";
  String _pesanCatatan = "";

  void printCatatan() {
    print("Judul: " + _judulCatatan);
    print("Pesan: " + _pesanCatatan);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('PeduliLindungi2.0'),
        ),
        body: Form(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(
                      "TAMBAH CATATAN PENYEDIA",
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: InkWell(
                        child: Text(
                          "Kembali ke Kumpulan Catatan",
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      )),
                  Padding(
                      padding: EdgeInsets.only(bottom: 15),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Ketik judul catatan di sini",
                          labelText: "Judul",
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        onChanged: (String? newVal) {
                          setState(() {
                            _judulCatatan = newVal!;
                          });
                        },
                        // validator: (_judulCatatan) {
                        //   if (_judulCatatan == "") {
                        //     return "Judul tidak boleh kosong";
                        //   }
                        //   return null;
                        // },
                      )),
                  Padding(
                      padding: EdgeInsets.only(bottom: 15),
                      child: TextFormField(
                        decoration: new InputDecoration(
                            hintText: "Ketik pesan catatan di sini",
                            labelText: "Pesan",
                            border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(5.0))),
                        onChanged: (String? newVal) {
                          setState(() {
                            _pesanCatatan = newVal!;
                          });
                        },
                        // validator: (_pesanCatatan) {
                        //   if (_pesanCatatan == "") {
                        //     return "Pesan tidak boleh kosong";
                        //   }
                        //   return null;
                        // },
                      )),
                  Container(
                    height: 50.0,
                    margin: EdgeInsets.all(10),
                    child: RaisedButton(
                      onPressed: () {
                        printCatatan();
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                            ),
                            borderRadius: BorderRadius.circular(30.0)),
                        child: Container(
                          constraints:
                              BoxConstraints(maxWidth: 250.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Tambah",
                            style: Theme.of(context).textTheme.headline3,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
