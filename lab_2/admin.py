from django.contrib import admin
from .models import Note

# Register models
admin.site.register(Note)