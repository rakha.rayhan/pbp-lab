from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index),
    # add xml path and json path
    path('xml', xml),
    path('json', json),
]