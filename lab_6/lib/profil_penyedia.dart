import 'package:flutter/material.dart';

class profil_penyedia extends StatelessWidget {
  final String title;
  const profil_penyedia({Key? key, required this.title}) : super(key: key);
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Padding(
          padding: EdgeInsets.only(bottom: 5),
          child: Text(
            "PROFIL PENYEDIA",
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: InkWell(
              child: Text(
                "Ubah Data",
                style: Theme.of(context).textTheme.headline2,
              ),
            )),
        Text(
          "NAMA INSTANSI",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: Text(
            "RS Pondok Indah",
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Text(
          "KOTA",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: Text(
            "DKI Jakarta",
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Text(
          "NOMOR TELEPON",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: Text(
            "021 1234567",
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Text(
          "ALAMAT",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: Text(
            "Pondok Indah",
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
        Container(
          height: 50.0,
          margin: EdgeInsets.all(10),
          child: RaisedButton(
            onPressed: () {},
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80.0)),
            padding: EdgeInsets.all(0.0),
            child: Ink(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                constraints: BoxConstraints(maxWidth: 250.0, minHeight: 50.0),
                alignment: Alignment.center,
                child: Text(
                  "Lihat Pendaftar",
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 50.0,
          margin: EdgeInsets.all(10),
          child: RaisedButton(
            onPressed: () {},
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(80.0)),
            padding: EdgeInsets.all(0.0),
            child: Ink(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                constraints: BoxConstraints(maxWidth: 250.0, minHeight: 50.0),
                alignment: Alignment.center,
                child: Text(
                  "Catatan Penyedia",
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.only(top: 10),
            child: InkWell(
              child: Text(
                "klik untuk lihat pesan bagi penyedia",
                style: Theme.of(context).textTheme.headline2,
              ),
            ))
      ])),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: 'Tambah Vaksin',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profil',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.exit_to_app),
            label: 'Log Out',
          ),
        ],
        currentIndex: 2,
      ),
    );
  }
}
